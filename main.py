from typing import Union
from fastapi import FastAPI


import numpy as np
from pydantic import BaseModel

import joblib

app = FastAPI()
from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

# Define allowed origins
origins = [
    "http://localhost",
    "http://localhost:8080",
    "https://example.com",
]

# Add CORS middleware
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "DELETE"],
    allow_headers=["*"],
)
model = joblib.load('Body_Fat_Estimator.pkl')

class InputData(BaseModel):
    weight: float
    neck: float
    abdomen: float
    biceps: float


@app.post("/predict")
def predict(data: InputData):
    features = np.array([data.weight, data.neck, data.abdomen, data.biceps])
    features = features.reshape(1, -1)
    prediction = model.predict(features)
    return {prediction[0]}


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Union[str, None] = None):
    return {"item_id": item_id, "q": q}